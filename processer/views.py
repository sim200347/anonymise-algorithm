import os
import shutil
from os import path

from django.core.files.storage import FileSystemStorage
from django.shortcuts import render

from algorythm import main


def get_page(request):
    for file in os.listdir("media/to_user"):
        os.remove('media/to_user/' + file)
    if request.method == 'POST':
        file = request.FILES['file_upload']
        fs = FileSystemStorage()
        fs.save(f'for_processing/{file.name}', file)
        main()
        src = path.realpath(f'./media/result/{file.name}')
        root_dir, _ = path.split(src)
        result_name = file.name[:-4]
        print(file.name)
        shutil.make_archive(f'media/to_user/{result_name}', 'zip', root_dir)
        file_url = fs.url(f'../media/to_user/{result_name}.zip')
        for file in os.listdir("media/result"):
            os.remove('media/result/' + file)
        for file in os.listdir('media/jpg'):
            os.remove('./media/jpg/' + file)
        for file in os.listdir('media/pdfs'):
            os.remove('./media/pdfs/' + file)
        return render(request, 'after.html', {'file_url': file_url})
    else:
        return render(request, 'before.html')
