from django.db import models


class FileUpload(models.Model):
    title = models.CharField(max_length=100)
    book = models.FileField(upload_to='files/')

    def __str__(self):
        return self.title
