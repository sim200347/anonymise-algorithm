from django.apps import AppConfig


class ProcesserConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'processer'
