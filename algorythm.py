import os
import re
import shutil

import cv2
import fitz
import pandas as pd
import pytesseract
import ru_core_news_md
import stanza
from PIL import Image
from natasha import Doc, PER
from natasha import (
    Segmenter,
    MorphVocab,
    NewsEmbedding,
    NewsMorphTagger,
    NewsSyntaxParser,
    NewsNERTagger,
    NamesExtractor
)
from pytesseract import Output
from pytesseract import pytesseract

REGEX = r"«.*\s?.*?»|\".*\s?.*?\"|\'.*\s?.*?\'|Гос.*|Деп.*|имени.*|Союза.*|Председ.*|Федеральн.*"
ZOOM = 2
THICKNESS = 2
INDENTION = 2
PATH = './media/pdfs'
GREEN = (0, 255, 0)
MAT = fitz.Matrix(ZOOM, ZOOM)
SEGMENT = Segmenter()
MORPH_VOCAB = MorphVocab()
EMB = NewsEmbedding()
MORPH_TAGGER = NewsMorphTagger(EMB)
SYNTAX_PARSER = NewsSyntaxParser(EMB)
NER_TAGGER = NewsNERTagger(EMB)
NAMES_EXTRACTOR = NamesExtractor(MORPH_VOCAB)


def jpg_to_pdf():
    # convert jpg to pdf for further work
    for element in os.listdir("media/jpg/"):
        image1 = Image.open("media/jpg/" + "/" + element)
        im1 = image1.convert('RGB')
        im1.save(r'./media/pdfs/{}.pdf'.format(element[:-4]))


def blur(image, x, y, h, w):
    # blur word by it position
    tmp = image[y:y + h, x:x + w]
    tmp = cv2.GaussianBlur(tmp, (23, 23), 30)
    image[y:y + tmp.shape[0], x:x + tmp.shape[1]] = tmp
    return image


def draw_rectangles(data, occ, image_copy):
    # set word position for blur
    w = data["width"][occ]
    h = data["height"][occ]
    x = data["left"][occ]
    y = data["top"][occ]
    image_copy = blur(image_copy, x, y, h, w)
    return image_copy


def get_grayscale(image):
    # make image gray
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)


def get_df_page(df_temp, page, file):
    # get pages
    df_page_temp = df_temp.query('(page == @page) & (file == @file)')
    df_page_temp.reset_index(drop=True)
    return df_page_temp


def get_image(file, page):
    # get image to anonymize
    doc = fitz.open(PATH + '/' + file)
    page_num = doc.load_page(int(page) - 1)
    pix = page_num.get_pixmap(matrix=MAT)
    pix.save('./new_file.jpg')
    image = cv2.imread('./new_file.jpg')
    gray = get_grayscale(image)
    return image


def save_image(file, page, image):
    # saving anonymized image
    page_name = './media/result/' + str(file[:-4]) + '_page_' + str(page) + "_anonymized" + '.jpg'
    print(page_name)
    cv2.imwrite(page_name, image)


def get_frames(doc, page, lang='rus', config="--psm 12"):
    # get words frames
    page = doc.load_page(page)
    pix = page.get_pixmap(matrix=MAT)
    pix.save('new_file.jpg')
    image = cv2.imread('new_file.jpg')
    return pytesseract.image_to_data(get_grayscale(image), output_type=Output.DATAFRAME, lang=lang, config=config)


def get_coordinates(temp_df, block, pn):
    # get parameters of all words
    temp_df2 = temp_df.query('par_num == @pn')
    min_left = temp_df2['left'].min()
    min_top = temp_df2['top'].min()
    max_width = temp_df2['width'].max()
    max_height = temp_df2['height'].max()
    return block, pn, min_left, min_top, max_width, max_height


def get_position(temp_df, df_coordinates, element, current_page, block, pn):
    # get coordinates of all words
    temp_df2 = temp_df.query('par_num == @pn')
    string_text = ' '.join(w for w in temp_df2['text'])
    if string_text[-1] != '.':
        string_text += '.'
    cords = df_coordinates.query('(block == @block)and(par_num == @pn)').copy()
    cords = cords.reset_index(drop=True)
    return (element, current_page + 1, cords['l'][0], cords['t'][0],
            cords['w'][0], cords['h'][0],
            string_text)


def divide_to_frames(df_test, line=0, i=0, p=0):
    # parsing files
    for element in os.listdir(PATH):
        if element[-3:] == 'pdf':
            doc = fitz.open(PATH + '/' + element)
            print('document:', element, 'pages:', doc.pageCount)
            for current_page in range(0, doc.pageCount):
                # creating coordinates dataframe
                box_df = get_frames(doc, current_page)
                df_coordinates = pd.DataFrame(data=None, columns=['block', 'par_num', 'l', 't', 'w', 'h'])
                for block in box_df['block_num'].unique():
                    temp_df = box_df.query('block_num == @block')
                    for pn in temp_df['par_num'].unique():
                        df_coordinates.loc[line] = get_coordinates(temp_df, block, pn)
                        line += 1
                # creating text blocks dataframe
                box_df.dropna(subset=['text'], inplace=True)
                df_text_blocks = pd.DataFrame(data=None, columns=['file', 'page', 'left', 'top', 'width', 'height',
                                                                  'text'])
                for block in box_df['block_num'].unique():
                    temp_df = box_df.query('block_num == @block')
                    for pn in temp_df['par_num'].unique():
                        df_text_blocks.loc[p] = get_position(temp_df, df_coordinates, element, current_page, block, pn)
                        p += 1
                df_test = df_test.append(df_text_blocks)
                i += 1
    return df_test


def main():
    # separating files by extensions
    for element in os.listdir('media/for_processing'):
        if element[-3:] == 'jpg':
            shutil.move("media/for_processing/" + element, "media/jpg/" + element)
        elif element[-3:] == 'pdf':
            shutil.move("media/for_processing/" + element, "media/pdfs/" + element)
    jpg_to_pdf()
    # make dataframe for analyse
    df_test = pd.DataFrame(data=None, columns=['file', 'page', 'left', 'top', 'width', 'height', 'text'])
    df_test = divide_to_frames(df_test)
    # initializing nlp model
    nlp_spacy = ru_core_news_md.load()
    stanza.download('ru', processors='tokenize,ner')
    nlp_stanza = stanza.Pipeline('ru', processors='tokenize,ner')
    # parsing files with information
    for file in df_test['file'].unique():
        for page in df_test.query('file == @file')['page'].unique():
            df_page_temp = get_df_page(df_test, page, file)
            image_copy = get_image(file, page)
            # getting data from image
            data = pytesseract.image_to_data(image_copy, output_type=pytesseract.Output.DICT, lang='rus',
                                             config="--psm 12 --psm 6 --psm 3")
            for ind in list(df_page_temp.index):
                text = re.sub(REGEX, "", df_page_temp['text'][ind])
                doc = nlp_stanza(text)
                # find persons in text with stanza model
                for ent in doc.entities:
                    if ent.type == "PER":
                        target_words = ent.text.strip().split()
                        for target_word in target_words:
                            word_occurrences = [i for i, word in enumerate(data['text']) if word == target_word]
                            for occ in word_occurrences:
                                image_copy = draw_rectangles(data, occ, image_copy)
                doc = nlp_spacy(text)
                # find persons in text with spacy model
                for ent in doc.ents:
                    if ent.label_ == "PER" and ent.text.lower() != 'отдела' and ent.text.lower() != 'планов':
                        target_words = ent.text.strip().split()
                        for target_word in target_words:
                            word_occurrences = [i for i, word in enumerate(data['text']) if word == target_word]
                            for occ in word_occurrences:
                                image_copy = draw_rectangles(data, occ, image_copy)
                # find persons in text with natasha model
                doc = Doc(text)
                doc.segment(SEGMENT)
                doc.tag_morph(MORPH_TAGGER)
                doc.tag_ner(NER_TAGGER)
                for span in doc.spans:
                    if span.type == PER:
                        target_words = span.text.strip().split()
                        for target_word in target_words:
                            word_occurrences = [i for i, word in enumerate(data['text']) if word == target_word]
                            for occ in word_occurrences:
                                image_copy = draw_rectangles(data, occ, image_copy)
            save_image(file, page, image_copy)


if __name__ == '__main__':
    main()
